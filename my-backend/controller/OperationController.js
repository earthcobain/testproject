const Operation = require('../models/Operation')

const operationController = {
  async addOperation (req, res, next) {
    const payload = req.body
    console.log(payload)
    const operation = new Operation(payload)
    try {
      await operation.save()
      res.json(operation)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getOperations (req, res, next) {
    try {
      const operations = await Operation.find({})
      res.json(operations)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = operationController
