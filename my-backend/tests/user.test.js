const dbHandler = require('./db-handler')
const User = require('../models/User')

beforeAll(async () => {
  await dbHandler.connect()
})

afterEach(async () => {
  await dbHandler.clearDatabase()
})

afterAll(async () => {
  await dbHandler.closeDatabase()
})

const userComplete = {
  name: 'MAC',
  gender: 'M'
}

const userComplete2 = {
  name: 'MAC',
  gender: 'F'
}

const userInComplete1 = {
  name: '',
  gender: 'M'
}

const userInComplete2Alpha = {
  name: 'MA',
  gender: 'M'
}

const userInComplete3NoGen = {
  name: 'MAC',
  gender: ''
}

describe('User', () => {
  it('สามารถเพิ่ม user gender:M ได้ ', async () => {
    let error = null
    try {
      const user = new User(userComplete)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })

  it('สามารถเพิ่ม user gender:F ได้ ', async () => {
    let error = null
    try {
      const user = new User(userComplete2)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).toBeNull()
  })

  it('ไม่สามารถเพิ่ม user ได้เพราะ name เป็นช่องว่าง ', async () => {
    let error = null
    try {
      const user = new User(userInComplete1)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })

  it('ไม่สามารถเพิ่ม user ได้เพราะ name น้อยกว่า 3 ตัว ', async () => {
    let error = null
    try {
      const user = new User(userInComplete2Alpha)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })

  it('ไม่สามารถเพิ่ม user ได้เพราะ gender เป็นช่องว่าง ', async () => {
    let error = null
    try {
      const user = new User(userInComplete3NoGen)
      await user.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })

  it('ไม่สามารถเพิ่ม user ที่ name ซ้ำ ', async () => {
    let error = null
    try {
      const user1 = new User(userComplete)
      await user1.save()
      const user2 = new User(userComplete)
      await user2.save()
    } catch (e) {
      error = e
    }
    expect(error).not.toBeNull()
  })
})
