const express = require('express')
const router = express.Router()
const operationController = require('../controller/OperationController')

router.get('/', operationController.getOperations)

router.post('/', operationController.addOperation)

module.exports = router
