const express = require('express')
const router = express.Router()
const usersController = require('../controller/UserController')
const auth = require('../config/auth')

router.post('/register', usersController.registerNewUser)
router.post('/login', usersController.loginUser)
router.get('/me', auth, usersController.getUserDetails)

module.exports = router
